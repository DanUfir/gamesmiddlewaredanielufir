﻿using UnityEngine;
using System.Collections;
using System;

public class PhysicsBounce : MonoBehaviour
{


    public Vector3 acceleration;
    public Vector3 velocity;
    private float cor = .8f;
    public float Mass = 1f; 

    public Vector3 n;
    public bool velocityChangedByOther = false;
    public Vector3 posOfBall;
   
    public PhysicsBounce otherPhy;
    // Use this for initialization

    void Start()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        acceleration = Vector3.zero;
        acceleration += Physics.gravity;

        velocity += acceleration * Time.deltaTime;
        transform.position += velocity * Time.deltaTime;      
        posOfBall = transform.position;

        
    }

    void OnTriggerStay(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            Application.LoadLevel(1);
        }
        if ((col.gameObject.tag == "Sphere"))
        {
            Vector3 vNorm = col.transform.position - this.transform.position;
            vNorm = vNorm.normalized;

            Vector3 velPar = ParallelComponent(velocity, vNorm);
            Vector3 velPerp = PerpComponent(velocity, vNorm);
            velocity = velPerp - (velPar * cor);
            
            if (!velocityChangedByOther)
            {

                otherPhy = col.GetComponent<PhysicsBounce>();
                otherPhy.velocityChangedByOther = true;
                
                velocity = calcMomentum(velocity, otherPhy.velocity, Mass, otherPhy.Mass);

            }
        }
        else
        {

            float distanceToPlane = Vector3.Dot((col.transform.position - this.transform.position), col.transform.up) * -1;

            float overlap = getRadius() - distanceToPlane;

            this.transform.position += (2 * overlap) * col.transform.up;

            Vector3 velPar = ParallelComponent(velocity, col.transform.up);
            Vector3 velPerp = PerpComponent(velocity, col.transform.up);
            velocity = velPerp - (velPar * cor);
        }
    }
    private void setVelocity(Vector3 vector3)
    {
        velocity = vector3;
        velocityChangedByOther = true;
    }

    private float getRadius()
    {
        return this.transform.localScale.x / 2;
    }
    
    Vector3 ParallelComponent(Vector3 target, Vector3 basis)
    {
        Vector3 n = basis.normalized;
        return Vector3.Dot(target, n) * n;
    }
    Vector3 PerpComponent(Vector3 target, Vector3 basis)
    {
        return target - ParallelComponent(target, basis);
    }
    
    private Vector3 calcMomentum(Vector3 velocity1, Vector3 velocity2, float mass1, float mass2)
    {
        Vector3 momentum1;
        Vector3 momentum2;
        momentum1 = ((mass1 - mass2 / mass1 + mass2) * velocity) + (((2 * mass2) / (mass1 + mass2)) * velocity2);

        momentum2 = ((mass2 - mass1 / mass1 + mass2) * velocity2) + (((2 * mass1) / (mass1 + mass2)) * velocity);

        otherPhy.velocity = momentum2;

        return momentum1;
    }
}
