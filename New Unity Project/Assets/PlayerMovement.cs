﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    private float verticSpeed;
    private float horizoSpeed;
    public bool crouching = false;
    CharacterController charControl;
    public bool lockMovement = false;

    //Camera
    private float mouseSpeed = 10f;
    private float minMaxRange = 85f;
    private float hRotation;
    private float vRotation;
    private float turningSpeed;
    // Use this for initialization
    void Start () {
        charControl = GetComponent<CharacterController>();

    }

    // Update is called once per frame
    void Update () {
        //Movement and rotation
        hRotation = Input.GetAxis("Mouse X") * mouseSpeed;
        transform.Rotate(0, hRotation, 0);

        vRotation -= Input.GetAxis("Mouse Y") * mouseSpeed;
        vRotation = Mathf.Clamp(vRotation, -minMaxRange, minMaxRange);

        Camera.main.transform.localRotation = Quaternion.Euler(vRotation, 0, 0);

        verticSpeed = Input.GetAxis("Vertical");
        verticSpeed *= 4;


        horizoSpeed = Input.GetAxis("Horizontal");
        horizoSpeed *= 4;

        Vector3 movementSpeed = new Vector3(horizoSpeed, 0, verticSpeed);

        movementSpeed = transform.rotation * movementSpeed;

        charControl.SimpleMove(movementSpeed * 2);

    }



}
